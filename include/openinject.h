#ifndef OPENINJ_H
#define OPENINJ_H

#include<stdint.h>
#include<netinet/in.h>
#include<stdint.h>
#include<arpa/inet.h>
#include<linux/if_packet.h>
#include<sys/socket.h>

struct arp_hdr {
    uint16_t htype; /* Hardware type */
    uint16_t ptype; /* Protocol type */
    uint8_t hlen;   /* Hardware address len */
    uint8_t plen;   /* Protocl address len */
    uint16_t op;    /* ARP OP code */
    uint8_t sha[6]; /* Source hardware address */
    uint8_t spa[4]; /* Source protocol addres */
    uint8_t tha[6]; /* Target hardware address */
    uint8_t tpa[4]; /* Target protocol address */
} __attribute__((packed));

/* Array size for error messages */
#define OPENINJ_ERRBUF_SIZE 200
#define OPENINJ_PACKET_SIZE 65536

/* Flags are used to keep track of what layers have already been constructed */
struct __openinj_flags {
    unsigned int ETHER: 1;
    unsigned int ARP: 1;
    unsigned int TCP: 1;
    unsigned int UDP: 1;
    unsigned int IP4: 1;
    unsigned int IP6: 1;
    unsigned int ICMP4: 1;
    unsigned int ICMP6: 1;
};

/* Context for the OpenInject session, contains
 * the memory space for datagram assembly, the name
 * of device used for injection.
 */
struct openinj_frame {
    uint8_t data[65536];
    int framelen;
    struct __openinj_flags flags;
};

typedef struct openinj_frame openinj_frame;

struct openinject_ctx {
    char device_name[100];
    int device_adfamily;
    int sfd;
    struct sockaddr_ll addr;
    struct sockaddr *ifa_addr;
    struct sockaddr *ifa_netmask;
    char errbuff[OPENINJ_ERRBUF_SIZE];
    
};

typedef struct openinject_ctx OPENINJ_CTX;

OPENINJ_CTX *openinj_ctx_init();
openinj_frame openinj_frame_init();
void openinj_ctx_free(OPENINJ_CTX *ctx);
int openinj_open_dev(OPENINJ_CTX *ctx, char *dev);
int openinj_inject(OPENINJ_CTX *ctx, openinj_frame *frame);
int openinj_get_device_mac(char *interface, uint8_t *mac);
int openinj_get_dev_ipv4_addr(OPENINJ_CTX *ctx, struct sockaddr_in *addr);
int openinj_get_dev_ipv6_addr(OPENINJ_CTX *ctx, struct sockaddr_in6 *addr);
int openinj_macaddr(unsigned char *dest, char *str);
void openinj_print_err(OPENINJ_CTX *ctx);
uint16_t openinj_rfc791_checksum(uint8_t *header, int header_length);

int openinj_build_ethernet(
        openinj_frame *frame,
        uint8_t *dest_addr,
        uint8_t *src_addr,
        uint16_t eth_type);

int openinj_build_arp(
        openinj_frame *frame,
        uint16_t htype,
        uint16_t ptype,
        uint8_t hlen,
        uint8_t plen,
        uint16_t op,
        uint8_t *sha,
        uint8_t *spa,
        uint8_t *tha,
        uint8_t *tpa);

int openinj_build_ipv4(openinj_frame *frame,
                       uint8_t version,
                       uint8_t ihl,
                       uint8_t tos,
                       uint16_t totlen,
                       uint16_t id,
                       uint16_t fragoff,
                       uint8_t ttl,
                       uint8_t proto,
                       uint32_t source,
                       uint32_t dest,
                       uint8_t *options,
                       int options_len);

void openinj_recalc_ipv4(openinj_frame *frame);

int openinj_build_ipv6(openinj_frame *frame,
                        uint8_t version,
                        uint8_t traffic_class,
                        uint32_t flow_label,
                        uint16_t payload_len,
                        uint8_t next_header,
                        uint8_t hop_limit,
                        uint8_t *s_addr,
                        uint8_t *d_addr);


int openinj_build_icmpv4(openinj_frame *frame,
        uint8_t type,
        uint8_t code,
        uint16_t content
        );

int openinj_build_icmpv6(openinj_frame *frame,
        uint8_t type,
        uint8_t code,
        uint16_t content
        );


#endif
