#include<openinject.h>
#include<stdio.h>
#include<stdint.h>

int main()
{
    uint16_t packet[10] = {0x4500, 0x0073, 0x0000, 0x4000, 0x4011, 
                           0x0000, 0xc0a8, 0x0001, 0xc0a8, 0x00c7 };
    uint16_t checksum;
    uint16_t expected = 0xb861;
    int passed;

    checksum = openinj_rfc791_checksum((uint8_t *)packet, 20);

    passed = checksum == expected ? 1 : 0;

    if(passed)
        printf("Checksum correct: 0x%04X\n", checksum);
    else
        printf("ERROR: expected 0x%04X, got: 0x%04X\n", expected, checksum);

    return passed;
}
