#include <netinet/in.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<ifaddrs.h>
#include<net/if.h>
#include<sys/socket.h>
#include<sys/ioctl.h>
#include<linux/if_ether.h>
#include<openinject.h>

/* Finds the device specified by dev the system for injection.
 * returns 0 on success and -1 on failure. If a device is found
 * a raw socket is opened bound to that device.
 * If dev is NULL the first IPv4 or IPv6 device found is used
 */
int openinj_open_dev(OPENINJ_CTX *ctx, char *dev) 
{
    struct ifaddrs *addrs;

    /* Create a linked list of all addresses on the
     * system 
     */
    if(getifaddrs(&addrs) == -1) {
        strcpy(ctx->errbuff, "Error in getifaddrs()");
        return 0;
    }

    struct ifaddrs *ptr = addrs;

    /* Loop through each address in the linked list
     * until an IPv4 or IPv6 device is found.
     */
    while(ptr) {
        /* Tunnel devices usually have a null ifa_addr
         * And skip if this is the device we already have in the context */
        if(ptr->ifa_addr != NULL &&
                (dev == NULL || strcmp(ctx->device_name, ptr->ifa_name)) != 0) {
            int addr_fam = ptr->ifa_addr->sa_family;
            /* If an IPv4 or IPv6 device and is not a loopback interface */
            if((addr_fam == AF_INET || addr_fam == AF_INET6)
                    && !(ptr->ifa_flags & IFF_LOOPBACK)) {
                /* Copy the device name and address family into
                 * the ctx
                 */
                ctx->device_adfamily = addr_fam;
                strcpy(ctx->device_name, ptr->ifa_name);
                memcpy(ctx->ifa_addr, ptr->ifa_addr, sizeof(struct sockaddr));
                memcpy(ctx->ifa_netmask, ptr->ifa_netmask, sizeof(struct sockaddr));

                /* Open the socket and bind it to the device */
                if((ctx->sfd = socket(AF_PACKET, SOCK_RAW, 0)) == -1){
                    strcpy(ctx->errbuff, "Error in socket()");
                    return -1;
                }

                /* Bind socket to interface using setsockopt(), use the sfd in
                * the ctx as the descriptor, SOL_SOCKET as the level, SO_BINDTODEVICE 
                * as the option name, the device name as the option value
                * and the length of the device name as the option length
                */
                if(setsockopt(ctx->sfd, SOL_SOCKET, SO_BINDTODEVICE, 
                            ctx->device_name, strlen(ctx->device_name)) == -1) {
                    strcpy(ctx->errbuff, "Failed to bind socket to device");
                    return -1;
                }

                return 0;
            }
        }
        ptr = ptr->ifa_next;
    }

    strcpy(ctx->errbuff, "No devices found.");
    return -1;

}

/* Fills addr with ipv4 address information about the device
 * opened inside ctx.
 */
int openinj_get_dev_ipv4_addr(OPENINJ_CTX *ctx, struct sockaddr_in *addr)
{
    struct ifaddrs *addrs;

    /* Create a linked list of all addresses on the
     * system 
     */
    if(getifaddrs(&addrs) == -1) {
        strcpy(ctx->errbuff, "Error in getifaddrs()");
        return -1;
    }

    struct ifaddrs *ptr = addrs;

    /* Loop through each address in the linked list
     * until dev is found or the linked list is exhausted
     */
    while(ptr) {
        int addr_fam = ptr->ifa_addr->sa_family;
        /* If the name of the device and dev are equal and this is an IPv4 device */
        if(!strcmp(ctx->device_name, ptr->ifa_name) && addr_fam == AF_INET) {
            memset(addr, 0, sizeof(struct sockaddr_in));
            memcpy(addr, ptr->ifa_addr, sizeof(struct sockaddr_in));
            return 0;
        }
        ptr = ptr->ifa_next;
    }
    return -1;
}

/* Fills addr with ipv6 address information about the device
 * opened inside ctx.
 */
int openinj_get_dev_ipv6_addr(OPENINJ_CTX *ctx, struct sockaddr_in6 *addr)
{
    struct ifaddrs *addrs;

    /* Create a linked list of all addresses on the
     * system 
     */
    if(getifaddrs(&addrs) == -1) {
        strcpy(ctx->errbuff, "Error in getifaddrs()");
        return -1;
    }

    struct ifaddrs *ptr = addrs;

    /* Loop through each address in the linked list
     * until dev is found or the linked list is exhausted
     */
    while(ptr) {
        int addr_fam = ptr->ifa_addr->sa_family;
        /* If the name of the device and dev are equal and this is an IPv6 device */
        if(!strcmp(ctx->device_name, ptr->ifa_name) && addr_fam == AF_INET6) {
            memset(addr, 0, sizeof(struct sockaddr_in6));
            memcpy(addr, ptr->ifa_addr, sizeof(struct sockaddr_in6));
            return 0;
        }
        ptr = ptr->ifa_next;
    }
    return -1;
}

/* Converts a single hex character to binary
 */
static char _hextoint(char c)
{
    if(c >= '0' && c <= '9')
        return c - '0';
    if(c >= 'a' && c <= 'f')
        return c - 'a' + 10;
    if(c >= 'A' && c <= 'F')
        return c - 'A' + 10;
    return -1;
}

/* Gets the MAC address of the device specified by the
 * interface parameter.
 * Returns 0 if interface could be found
 * Returns -1 otherwise
 */
int openinj_get_device_mac(char *interface, uint8_t *mac)
{
    int sfd;
    struct ifreq ifr;
    sfd = socket(AF_INET, SOCK_DGRAM, 0);
    strcpy(ifr.ifr_name, interface);
    if(ioctl(sfd, SIOCGIFHWADDR, &ifr) == -1) {
        close(sfd);
        return -1;
    }     

    close(sfd);     
    memcpy(mac, ifr.ifr_hwaddr.sa_data, ETH_ALEN);
    return 0;
}

/* Converts the colon seperated MAC address contained in
 * str to a 6 byte machine readable address in dest
 */
int openinj_macaddr(unsigned char *dest, char *str)
{
    /* A colon notation mac address AA:BB:CC:DD:EE:FF is
     * exactly 17 chars in length, if the user has supplied
     * a str that is not 17 chars then it is invalid.
     */
    if(strlen(str) == 17) {
        char *ptr = str;
        int i = 0;
        while(i < 6) {
            char hex;
            /* If hextoint returns -1 then character is not valid */
            if((hex = _hextoint(*ptr++)) == -1)
                return 0;
            unsigned char byte = hex << 4;
            if((hex = _hextoint(*ptr++)) == -1)
                return 0;
            byte += hex;
            /* Skip colon */
            ptr++;
            dest[i++] = byte;
        }
    }
    else
        return -1;

    return 0;
}
