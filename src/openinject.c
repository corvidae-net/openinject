#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<net/if.h>
#include<sys/ioctl.h>
#include<openinject.h>

/* Initalises an openinject context. Returns a pointer
 * to the reserved space for the context in memory.
 * Space should be freed openinj_ctx_free()
 */
OPENINJ_CTX *openinj_ctx_init()
{
    OPENINJ_CTX *ctx = (OPENINJ_CTX *) malloc(sizeof(OPENINJ_CTX));
    ctx->ifa_addr = malloc(sizeof(struct sockaddr));
    ctx->ifa_netmask = malloc(sizeof(struct sockaddr));
    ctx->sfd = -1;
    return ctx;
}

openinj_frame openinj_frame_init()
{
    openinj_frame frame;
    frame.framelen = 0;
    memset(&frame.flags, 0, sizeof(frame.flags));
    return frame;
}

/* Frees the memory reserved by the openinject
 * context
 */
void openinj_ctx_free(OPENINJ_CTX *ctx)
{
    free(ctx->ifa_addr);
    free(ctx->ifa_netmask);
    free(ctx);
}

void openinj_print_err(OPENINJ_CTX *ctx)
{
    perror(ctx->errbuff);
}

/* Injects the packet built in ctx->packet. Return the size
 * of the packet inject in bytes if successful and -1 otherwise
 */
int openinj_inject(OPENINJ_CTX *ctx, openinj_frame *frame)
{
    /* If sfd is equal to -1 then a socket
     * hasn't been opened and bound to a device
     */
    if(ctx->sfd == -1) {
        strcpy(ctx->errbuff, "Device/socket not opened, use openinj_open_dev() \
                or openinj_open_next_dev()");
        return -1;
    }

    /* Retreive the index of the network interface so that we
     * can build the sockaddr_ll structure for the call to
     * sendto()
     */
     struct ifreq req;
     memset(&req, 0, sizeof(req));
     memset(&ctx->addr, 0, sizeof(struct sockaddr_ll));

    /* Copy device name into structure */
     strncpy(req.ifr_name, ctx->device_name, IF_NAMESIZE-1);

     /* Get the device index */
     if(ioctl(ctx->sfd, SIOGIFINDEX, &req) == -1) {
         strcpy(ctx->errbuff, "Error getting interface index ioctl()");
         return -1;
     }

    /* Set the index in the sockaddr_ll struct in ctx, we do not need to
     * set other link-layer information as we have assemble the ethernet
     * frame ourselves
     */
    ctx->addr.sll_ifindex = req.ifr_ifindex;

    if(!frame->flags.ETHER) {
        strcpy(ctx->errbuff, "No ethernet header.");
        return -1;
    }

    if(sendto(ctx->sfd, frame->data, frame->framelen, 0, 
                (struct sockaddr *) &ctx->addr, sizeof(struct sockaddr_ll)) == -1) {
        strcpy(ctx->errbuff, "Error in sendto()");
        return -1;
    }

    return frame->framelen; 
}
