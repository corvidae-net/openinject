#include<openinject.h>
#include<linux/ip.h>
#include<linux/ipv6.h>
#include<string.h>

#define ETH_HDR_LEN 14

int openinj_build_ipv6(openinj_frame *frame,
                        uint8_t version,
                        uint8_t traffic_class,
                        uint32_t flow_label,
                        uint16_t payload_len,
                        uint8_t next_header,
                        uint8_t hop_limit,
                        uint8_t *s_addr,
                        uint8_t *d_addr)
{
    struct ipv6hdr header;
    int header_len = sizeof(header);

    header.version = version;
    header.priority = traffic_class;
    memcpy(&header.flow_lbl, &flow_label, sizeof(header.flow_lbl));
    if(payload_len == 0)
        header.payload_len = htons((frame->framelen - ETH_HDR_LEN) + header_len);
    else
        header.payload_len = htons(payload_len);
    header.nexthdr = next_header;
    header.hop_limit = hop_limit;
    memcpy(&header.saddr, s_addr, sizeof(struct in6_addr));
    memcpy(&header.daddr, d_addr, sizeof(struct in6_addr));

    if(frame->flags.IP6 != 1) {
        frame->flags.IP6 = 1;
        frame->framelen += header_len;
    }

    memcpy(frame->data + ETH_HDR_LEN, &header, sizeof(struct ipv6hdr));
    return 0;
}
