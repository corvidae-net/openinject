#include<string.h>
#include<linux/if_ether.h>
#include<arpa/inet.h>
#include<openinject.h>

int openinj_build_ethernet(
        openinj_frame *frame,
        uint8_t *dest_addr,
        uint8_t *src_addr,
        uint16_t eth_type)
{
    struct ethhdr *packet_header = (struct ethhdr *)frame->data;
    memcpy(packet_header->h_dest, dest_addr, ETH_ALEN);
    memcpy(packet_header->h_source, src_addr, ETH_ALEN);
    packet_header->h_proto = htons(eth_type);


    /* If the ethernet flag has not already been set then
     * set it */
    if(frame->flags.ETHER != 1) {
        frame->flags.ETHER = 1;
        frame->framelen += ETH_HLEN;
    }

    return 0;
}
