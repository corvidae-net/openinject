#include<openinject.h>
#include<linux/icmp.h>
#include<linux/icmpv6.h>
#include<stdint.h>
#include<string.h>

int openinj_build_icmpv4(openinj_frame *frame,
        uint8_t type,
        uint8_t code,
        uint16_t content)
{
    /* Start of the header should point to the end of the current packet assembly */
    struct icmphdr header;

    int len = sizeof(struct icmphdr);

    /* Zero out everything */
    memset(&header, 0, len);

    header.code = code;
    header.type = type;
    /* Calculate rfc791 checksum */
    header.checksum = openinj_rfc791_checksum((uint8_t *)&header, len);

    memcpy(frame->data + frame->framelen, &header, len);

    /* Increase packet_len to reflect size of new assembly */
    if(frame->flags.ICMP4 != 1) {
        frame->flags.ICMP4 = 1;
        frame->framelen += len;
    }

    openinj_recalc_ipv4(frame);
    return 0;
}

int openinj_build_icmpv6(openinj_frame *frame,
        uint8_t type,
        uint8_t code,
        uint16_t content)
{
    /* Start of the header should point to the end of the current packet assembly */
    struct icmp6hdr header;

    int len = sizeof(struct icmp6hdr);

    /* Zero out everything */
    memset(&header, 0, len);

    header.icmp6_code = code;
    header.icmp6_type = type;
    /* Calculate rfc791 checksum */
    header.icmp6_cksum = openinj_rfc791_checksum((uint8_t *)&header, len);

    memcpy(frame->data + frame->framelen, &header, len);

    /* Increase packet_len to reflect size of new assembly */
    frame->framelen += len;
    openinj_recalc_ipv4(frame);
    return 0;
}
